package l10.tests;

import net.datastructures.ArrayQueue;
import net.datastructures.LinkedBinaryTree;
import net.datastructures.Position;
import net.datastructures.Queue;

public class TestCharRangeTree {

	public static void main(String[] args) {
		System.out.println("-------------------------------------------------------------");
		// Define&Create: ArrayStack
		LinkedBinaryTree<String> sBTree = new LinkedBinaryTree<String>();
		//
		Position<String> root = sBTree.addRoot("@Root_0");
		Queue<Position<String>> nodes = new ArrayQueue<Position<String>>(1000);
		System.out.println("aBTree.depth(root) = " + sBTree.depth(root));
		// Feed
		nodes.enqueue(root);
		int refelementCode = 64;
		while (!nodes.isEmpty() && sBTree.depth(nodes.first()) < 3) {
			Position<String> parentNode = nodes.dequeue();
			//
			char parentElement = parentNode.getElement().charAt(0);
			char leftChildElement = (char) (++refelementCode);
			char rightChildElement = (char) (++refelementCode);
			//
			Position<String> leftChild = sBTree.addLeft(parentNode, Character.toString(leftChildElement));
			Position<String> rightChild = sBTree.addRight(parentNode, Character.toString(rightChildElement));

			nodes.enqueue(leftChild);
			nodes.enqueue(rightChild);
		}
		// Manipulate&Process
		System.out.println("aBTree.positions : " + TreeUtils.toStringTreePositions(sBTree.positions()));
		System.out.println("aBTree.preorder : " + TreeUtils. toStringTreePositions(sBTree.preorder()));
		System.out.println("aBTree.postorder : " + TreeUtils.toStringTreePositions(sBTree.postorder()));
		System.out.println("aBTree.breadthfirst : " + TreeUtils.toStringTreePositions(sBTree.breadthfirst()));
		System.out.println("aBTree.inorder : " + TreeUtils.toStringTreePositions(sBTree.inorder()));
		// Printing Tree
		System.out.println("-------------------------------------------------------------");
		System.out.println(TreeUtils.toStringIndentTree(sBTree));
		System.out.println("-------------------------------------------------------------");
		

	}
}
// https://www.baeldung.com/java-string-formatter //
