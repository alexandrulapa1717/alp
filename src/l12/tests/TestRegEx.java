package l12.tests;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestRegEx {

	public static void main(String[] args) {
		System.out.println("---------------------------------------------");
		System.out.println("Test Java RegEx API");
		System.out.println("---------------------------------------------");
		String text = "InformaticaEconomica.org";
		String patternString = ".or";
		
		// Create a pattern to be searched
		Pattern pattern = Pattern.compile(patternString);

		// Search above pattern
		Matcher m = pattern.matcher(text);

		// Print starting and ending indexes of the pattern
		// in text
		while (m.find())
			System.out.println("Sablonul (" + pattern 
					+ ") gasit in textul " + text + " de la pozitia: "
					+ m.start() + " la pozitia " + (m.end() - 1));

		System.out.println("---------------------------------------------");
		System.out.println("Test Java String RegEx");
		System.out.println("---------------------------------------------");
		String numberString = "12.2568";
		patternString = "^(\\d*\\.)?\\d+$";
		System.out.println("Sirul numeric " + numberString 
				+  " este conform cu (" + patternString + ")? "
				+ numberString.matches(patternString));
		numberString = "abc123.258";
		System.out.println("Sirul numeric " + numberString 
				+  " este conform cu (" + patternString + ")? "
				+ numberString.matches(patternString));
		System.out.println("---------------------------------------------");

	}

}
