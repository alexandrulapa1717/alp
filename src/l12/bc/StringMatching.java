package l12.bc;

import net.datastructures.Map;
import net.datastructures.UnsortedTableMap;

public class StringMatching {

	public static void main(String[] args) {
		String text = "InformaticaEconomica.org";
		String patternString = "or";
		
		Map<Integer, Integer> matches = multipleMatching(text,patternString);
		
		for(Integer i: matches.keySet())
			System.out.println(i + " -> " + matches.get(i));

	}

	public static Map<Integer, Integer> multipleMatching(String textString, String patternString) {
		char[] text = textString.toCharArray();
		char[] pattern = patternString.toCharArray();
		Map<Integer, Integer> matches = new UnsortedTableMap<Integer, Integer>(); 
		int n = text.length;
		int m = pattern.length;
		for (int i = 0; i <= n - m; i++) {
			int k = 0;
			while (k < m && text[i + k] == pattern[k])
				k++;
			if (k == m) { 
				matches.put(i, i+k);
			}
		}
		return matches;
	}	
}
