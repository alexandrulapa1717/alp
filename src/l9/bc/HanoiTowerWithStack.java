package l9.bc;

import net.datastructures.ArrayStack;
import net.datastructures.Stack;

/* Stack Business Case*/
public class HanoiTowerWithStack {

	public static void main(String[] args) {
		System.out.println("Turnurile din Hanoi:"); 
		Stack<String> stack_A = new ArrayStack<String>();
		Stack<String> stack_B = new ArrayStack<String>();
		Stack<String> stack_C = new ArrayStack<String>();
		
		stack_A.push("disc.1____");
		stack_A.push("disc.2___");
		stack_A.push("disc.3__");
		stack_A.push("disc.4_");
		
		System.out.println("Stack_A: " + stack_A);
		System.out.println("Stack_B: " + stack_B);
		System.out.println("Stack_C: " + stack_C);
		
		Hanoi(stack_A.size(), stack_A, stack_C, stack_B);
		
		System.out.println("Stack_A: " + stack_A);
		System.out.println("Stack_B: " + stack_B);
		System.out.println("Stack_C: " + stack_C);
	}
	
	public static <E> void Hanoi(int n, Stack<E> src, Stack<E> dst, Stack<E> tmp) { 
		if (n > 0) {
			// muta toate discurile cu exceptia celui mai mare in tmp
			Hanoi(n - 1, src, tmp, dst);
			
			// muta discul cel mai mare (ultimul, adica discul n) in destinatie
			E disk = src.top();
			System.out.println("Muta disc: " + disk + " de la " + src + " la " + dst);
			dst.push(src.pop());
			// System.out.println("Mutat disc." + n + " de la " + src + " la " + dst);
			
			// recursiv: muta toate discurile din tmp in destinatie (peste discul cel mai mare)
			// folosing src ca si tmp de aceasta data
			Hanoi(n - 1, tmp, dst, src);
		}
	}
	
}
