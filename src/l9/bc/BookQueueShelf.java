package l9.bc;

import net.datastructures.ArrayList;
import net.datastructures.ArrayQueue;
import net.datastructures.List;
import net.datastructures.Queue;

public class BookQueueShelf {
	Queue<Book> shelfQueue = new ArrayQueue<Book>();
	Integer shelfWidth;
	Integer shelfAllBooksWidth = 0;
	
	List<Book> addBookInShelf(Book b) {
		List<Book> releasedBooks = new ArrayList<Book>();
		//
		if (this.shelfAllBooksWidth + b.width <= this.shelfWidth) {
			shelfQueue.enqueue(b);
			shelfAllBooksWidth += b.width;
		}else {
			while(this.shelfWidth - this.shelfAllBooksWidth < b.width) {
				Book releasedBook = shelfQueue.dequeue();
				releasedBooks.add(releasedBooks.size(), releasedBook);
				shelfAllBooksWidth -= releasedBook.width;
			}
		}
		return releasedBooks;
	}
	void printBooksFromShelf() {
		System.out.println(shelfQueue);
	}
	public BookQueueShelf(Integer shelfWidth) {
		this.shelfWidth = shelfWidth;
	}
	public static void main(String[] args) {
		BookQueueShelf shelf = new BookQueueShelf(75);
		List<Book> releaseBooks;
//		shelf.printBooksFromShelf();
		releaseBooks = shelf.addBookInShelf(new Book("B1", 25));
		shelf.printBooksFromShelf();
		System.out.println("Released: " + releaseBooks);
		releaseBooks = shelf.addBookInShelf(new Book("B2", 35));
		shelf.printBooksFromShelf();
		System.out.println("Released: " + releaseBooks);
		releaseBooks = shelf.addBookInShelf(new Book("B3", 15));
		shelf.printBooksFromShelf();
		System.out.println("Released: " + releaseBooks);
		releaseBooks = shelf.addBookInShelf(new Book("B4", 10));
		shelf.printBooksFromShelf();
		System.out.println("Released: " + releaseBooks);
		releaseBooks = shelf.addBookInShelf(new Book("B5", 25));
		shelf.printBooksFromShelf();
		System.out.println("Released: " + releaseBooks);
		releaseBooks = shelf.addBookInShelf(new Book("B6", 20));
		shelf.printBooksFromShelf();
		System.out.println("Released: " + releaseBooks);
	}
}
//--------------------------------------
class Book{
	String title;
	Integer width;
	public Book(String title, Integer width) {
		super();
		this.title = title;
		this.width = width;
	}
	@Override
	public String toString() {
		return "Book [title=" + title + ", width=" + width + "]";
	}
	
	
}
