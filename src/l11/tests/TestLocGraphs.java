package l11.tests;

import net.datastructures.AdjacencyMapGraph;
import net.datastructures.Edge;
import net.datastructures.Graph;
import net.datastructures.Map;
import net.datastructures.SortedTableMap;
import net.datastructures.Vertex;

public class TestLocGraphs {

	public static void main(String[] args) {
		// Define:
		Graph<String,Integer> gLoc = new AdjacencyMapGraph<>(true);
		
		// Feed:
		// Add&Create vertices
		Map<String, Vertex<String> > verts = new SortedTableMap<>();
		verts.put("IS", gLoc.insertVertex("IS"));
		verts.put("BT", gLoc.insertVertex("BT"));
		verts.put("VS", gLoc.insertVertex("VS"));
		verts.put("PN", gLoc.insertVertex("PN"));
		verts.put("SV", gLoc.insertVertex("SV"));
		verts.put("BC", gLoc.insertVertex("BC"));
		verts.put("TGN", gLoc.insertVertex("TGN"));
		verts.put("DRH", gLoc.insertVertex("DRH"));
		verts.put("VDR", gLoc.insertVertex("VDR"));
		verts.put("CMD", gLoc.insertVertex("CMD"));
		
		// Add&Create edges
		gLoc.insertEdge(verts.get("IS"), verts.get("BT"), 120);
		gLoc.insertEdge(verts.get("IS"), verts.get("VS"), 70);
		gLoc.insertEdge(verts.get("PN"), verts.get("SV"), 80);
		gLoc.insertEdge(verts.get("IS"), verts.get("SV"), 110);
		gLoc.insertEdge(verts.get("SV"), verts.get("VDR"), 80);
		gLoc.insertEdge(verts.get("IS"), verts.get("PN"), 120);
		gLoc.insertEdge(verts.get("SV"), verts.get("CMD"), 30);
		gLoc.insertEdge(verts.get("PN"), verts.get("BC"), 100);
		gLoc.insertEdge(verts.get("PN"), verts.get("TGN"), 25);
		gLoc.insertEdge(verts.get("BT"), verts.get("DRH"), 30);
		gLoc.insertEdge(verts.get("DRH"), verts.get("SV"), 40);
		
		// Manipulate&Process
		System.out.println(gLoc);
		
		// Cauta varf IS
		String labelVarfIasi = "IS";
		Vertex<String> Iasi = null;
		for(Vertex<String> v: gLoc.vertices())
			if (v.getElement().equals(labelVarfIasi))
				Iasi = v;
		if (Iasi != null)
			System.out.println("Am gasit: " + Iasi.getElement());
		
		// Cauta varfuri accesibile din iasi
		if (Iasi != null)
			for (Edge<Integer> edge: gLoc.outgoingEdges(Iasi))
				System.out.println(" - cale spre " 
						+ gLoc.opposite(Iasi, edge).getElement() + ": " 
						+ edge.getElement());
	}

}
// GraphExamples
// GraphAlgorithms
// AdjacencyMapGraph