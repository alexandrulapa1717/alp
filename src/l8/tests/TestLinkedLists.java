package l8.tests;

import l8.bc.DoublyLinkedListScoreboard;

import dsaj.arrays.GameEntry;
import net.datastructures.CircularlyLinkedList;
import net.datastructures.DoublyLinkedList;

public class TestLinkedLists {
	
	public static void main(String[] args) {
		testLinkedListOps();
//		testLinkedListBoard();
	}
	
	static void testLinkedListOps() {
		// Define&Create: DoubleLinkedList
		DoublyLinkedList<String> dllList = new DoublyLinkedList<String>(); 
	
		// Feed
		for (int i = 0; i < 10; i++) {
			dllList.addLast("Element_" + i);
		}
		System.out.println("1 ########################");
		System.out.println("dllList: " + dllList);
		System.out.println("##########################\n");
		
		
		// Manipulate
		dllList.removeFirst();
		dllList.removeLast();
		dllList.addFirst("New_Element_0");
		System.out.println("2 ########################");
		System.out.println("dllList: " + dllList);
		System.out.println("##########################\n");
		
		
		// Process
		String dllFirstElement = dllList.first();
		System.out.println("3 ########################");
		System.out.println("dllFirstElement = " + dllFirstElement);
		System.out.println("##########################\n");
		
		
		
		
		// Define&Create: CircularlyLinkedList
		CircularlyLinkedList<String> cllList = new CircularlyLinkedList<>(); 
	
		// Feed
		for (int i = 0; i < 10; i++) {
			cllList.addLast("Element_" + i);
		}
		System.out.println("4 ########################");
		System.out.println("cllList: " + cllList);
		System.out.println("##########################\n");
		
		
		// Manipulate
		cllList.removeFirst();
//		cllList.removeLast();
		cllList.addLast("New_Element_10");
		System.out.println("5 ########################");
		System.out.println("cllList: " + cllList);
		System.out.println("##########################\n");
		
		
		// Process
		String cllLastElement = cllList.last();
		System.out.println("6 ########################");
		System.out.println("cllLastElement = " + cllLastElement);
		System.out.println("##########################\n");
	}
	
	static void testLinkedListBoard() {
		// The main method
		DoublyLinkedListScoreboard highscores = new DoublyLinkedListScoreboard(5);
		String[] names = { "Rob", "Mike", "Rose", "Jill", "Jack", "Anna", "Paul", "Bob" };
		int[] scores = { 750, 1105, 590, 740, 510, 660, 720, 400 };

		
		System.out.println("7 ########################");
		for (int i = 0; i < names.length; i++) {
			GameEntry gE = new GameEntry(names[i], scores[i]);
			System.out.println("Adding " + gE + "--- into [" + highscores + "]/" + highscores.getCapacity());
			highscores.add(gE);
			System.out.println(" Scoreboard new: " + highscores + "--- /" + highscores.getCapacity() + "\n");
		}
		System.out.println("##########################\n");
		
		System.out.println("8 ########################");
		System.out.println("Removing score at index " + 3);
		highscores.remove(3);
		System.out.println(highscores + "/" + highscores.getCapacity());
		
		
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		
		
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		
		
		System.out.println("Removing score at index " + 1);
		highscores.remove(1);
		System.out.println(highscores);
		
		
		System.out.println("Removing score at index " + 0);
		highscores.remove(0);
		System.out.println(highscores);
		System.out.println("##########################\n");
	}
}